<?php
/*
Plugin Name: UF CLAS - Auto Update
Plugin URI: http://it.clas.ufl.edu/
Description: Enables automatic background updates for core, plugins, and themes. See <a href="https://codex.wordpress.org/Configuring_Automatic_Background_Updates" target="_blank">Configuring Automatic Background Updates</a> for more details.
Version: 1.0.0
Author: Priscilla Chapman (CLAS IT)
Author URI: http://it.clas.ufl.edu/
License: GPL2
*/

// Enable automatic updates for core major releases
add_filter( 'allow_major_auto_core_updates', '__return_true' );

// Enable automatic updates for all plugins
add_filter( 'auto_update_plugin', '__return_true' );

// Enable automatic updates for all themes
add_filter( 'auto_update_theme', '__return_true' );