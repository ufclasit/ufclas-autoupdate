# UF CLAS - Auto Update #

Enables automatic background updates for core, plugins, and themes. See [Configuring Automatic Background Updates](https://codex.wordpress.org/Configuring_Automatic_Background_Updates) in the WordPress Codex for more details and options.

Updates will run every 12 hours by default. There are no notification emails or alerts in the dashboard when this happens. 

If you just need to know when updates are available, use a plugin like the [WP Update Notifier](https://wordpress.org/plugins/wp-updates-notifier/) instead.

### Requirements ###

* WordPress 3.7+
* FTP/SSH access to the WordPress installation
* Administrator access to WordPress (optional)

### Installation ###

WordPress recommends putting update files in a special [Must Use plugins](https://codex.wordpress.org/Must_Use_Plugins) folder called 'mu-plugins'. Plugins in this folder are automatically activated and cannot be installed or deactivated using the WordPress admin dashboard.  

* Use FTP/SSH and create a new folder 'mu-plugins' in wp-content. (wp-content/mu-plugins) 

* Upload the php file into the mu-plugins folder and that's it.

To confirm that the plugin is installed and working, log into the WordPress dashboard and go to Plugins. Next to the All, Active, and Inactive plugin links, you'll now see a 'Must-Use' link. Click the link to view all plugins in the Must-Use folder.

### Contributions ###

Your feedback and contributions are welcome and appreciated. Contact the repository owner.